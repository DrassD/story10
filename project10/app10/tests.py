from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.core import mail
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from .views import profile, subscribe, checkemail, createuser
from .models import Form
import time

class Story10TestCase(TestCase):
    def test_url_landing_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_function_landing(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, profile)

    def test_function_form(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)
    def test_template(self):
        response = Client().get('/subscribe/')
        self.assertTemplateUsed(response, 'form.html')
        response = Client().get('')
        self.assertTemplateUsed(response, 'profile.html')

    def test_send_email(self):
        mail.send_mail(
            'Subject here', 'Here is the message.',
            'from@example.com', ['to@example.com'],
            fail_silently=False,
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Subject here')
        
    def test_add_existing_email(self):
        Form.objects.create(email='asd@asd', name='asd@asd', password='asd@asd')
        response = Client().get('/checkemail'+ 'asd@asd')
        html_response = response.content.decode('utf8')
        self.assertIn("Not Found", html_response)
    
    def test_check_email(self):
        email = 'asd@asd'
        found = resolve('/checkemail/' + email)
        self.assertEqual(found.func, checkemail)